#coding=utf-8

import json
import time
import datetime

from app.db import *

from flask import  Blueprint,render_template, request, jsonify
from sqlalchemy.sql import select



bp_views = Blueprint('views', __name__, url_prefix="")


@bp_views.route('/api/v1/stores/time/get/<int:bussiness_id>', methods=['GET'])
def opening_hours(bussiness_id):
    bussiness_id = int(bussiness_id)
    
    hours = session.query(Bussiness_hours).filter(Bussiness_hours.shop_id \
                == bussiness_id)
    #if opening is None:
    #    pass
    hours_result = [hour.to_json() for hour in hours ]
    return jsonify(result=hours_result)


@bp_views.route('/api/v1/stores/status/get/<int:bussiness_id>', methods=['GET'])
def opening_status(bussiness_id):
    bussiness_id = int(bussiness_id)
    cur = time.strftime("%H:%M:%S", time.localtime())
    weekday = datetime.datetime.now().weekday() +1
    
    stat = session.query(Bussiness_hours).filter(Bussiness_hours.shop_id ==bussiness_id).filter(Bussiness_hours.opening_time >= cur).filter(cur <=  Bussiness_hours.closing_time).filter(Bussiness_hours.dayofweek_id == weekday).count()
    if stat:
        stat_result=True
    else:
        stat_result=False
    return jsonify(result=stat_result)
       

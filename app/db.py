# coding=utf-8
import datetime

from sqlalchemy import create_engine
from sqlalchemy import Table, Column, MetaData
from sqlalchemy import BigInteger, DateTime, String, CHAR, Text, Integer,ForeignKey
from sqlalchemy.types import Time, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session ,sessionmaker, relationship, backref
from sqlalchemy.pool import NullPool

import json


BaseModel = declarative_base()
#session_engine = create_engine()

session_engine = create_engine('mysql+pymysql://root:password@localhost/mydb', encoding="utf8",poolclass=NullPool, echo=True, )
Session = scoped_session(sessionmaker(session_engine))
session = Session()



class Bussiness_hours(BaseModel):
    __tablename__ = 'hours'
    
    id = Column(BigInteger, primary_key=True,autoincrement=True)
    shop_id = Column(BigInteger, nullable=False, index=True)
    dayofweek_id = Column(Integer,ForeignKey('dayofweek.id'), nullable=False)
    opening_time = Column(Time)
    closing_time = Column(Time)
    
    
    def to_json(self):
      return {
          "shop_id": self.shop_id,
          "dayofweek_id": self.dayofweek_id,
          "opening_time": str(self.opening_time),
          "closing_time": str(self.opening_time)

      }

class DAYOFWEEK(BaseModel):
    __tablename__ = 'dayofweek'

    id = Column(Integer, primary_key=True,autoincrement=True)
    name = Column(String(32), nullable=False)




if __name__ == '__main__':
	BaseModel.metadata.create_all(session_engine)


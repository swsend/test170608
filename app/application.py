#coding=utf-8
import os

from flask import Flask
from app.views import bp_views
from app.db import *



def create_app():
    app = Flask(__name__)
    app.register_blueprint(bp_views)
    with app.app_context():
        pass

    return app

app = create_app()


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=5000, debug=False)
    
    

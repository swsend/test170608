CREATE TABLE hours (
	id BIGINT NOT NULL AUTO_INCREMENT, 
	shop_id BIGINT NOT NULL, 
	dayofweek_id INTEGER NOT NULL, 
	opening_time TIME, 
	closing_time TIME, 
	PRIMARY KEY (id), 
	FOREIGN KEY(dayofweek_id) REFERENCES dayofweek (id)
)


CREATE TABLE dayofweek (
	id INTEGER NOT NULL AUTO_INCREMENT, 
	name VARCHAR(32) NOT NULL, 
	PRIMARY KEY (id)
)

insert into dayofweek (id, name) values (1, 'Mon'), (2,'Tue'), (3, 'Wed'), (4, 'Thurs'), (5,'Fri'), (6, 'Sat'),(7,'Sun');


